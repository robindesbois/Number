package com.example;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.activity.HomeActivity;

public class AdapterNumero extends BaseAdapter {
    private Context context;
    private final Integer[] numeros;

    public AdapterNumero(Context context, Integer[] numeros) {
        this.context = context;
        this.numeros = numeros;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = inflater.inflate(R.layout.cellule, null);

            TextView textView = (TextView) gridView
                    .findViewById(R.id.tvCelule);
            textView.setText(String.valueOf(numeros[position]));
            textView.setBackgroundColor(Color.WHITE);
            if(Integer.valueOf(textView.getText().toString()) == HomeActivity.numeroCliquer) {
                textView.setBackgroundColor(Color.GREEN);
            }

            if (HomeActivity.perdu) {
                if (Integer.valueOf(textView.getText().toString()) == HomeActivity.numeroCliquer)
                    textView.setBackgroundColor(Color.RED);
                if (Integer.valueOf(textView.getText().toString()) == HomeActivity.numberVoulu - 1)
                    textView.setBackgroundColor(Color.YELLOW);//
            }

        } else {
            gridView = convertView;
        }

        return gridView;
    }

    @Override
    public int getCount() {
        return numeros.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}