package com.example.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.AdapterNumero;
import com.example.R;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HomeActivity extends AppCompatActivity {

    private static final int FREQUENCE_RAFRAICHISSEMENT = 20;
    private static final long FREQUENCE_ALEATOIRE = 3000;
    public static int numeroCliquer;
    static Integer[] numeros = new Integer[]{
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    GridView gridView;
    public static int numberVoulu = 1;

    AdapterNumero adapter;
    Button btnRecommencer;
    RelativeLayout rl;
    TextView tvClique;
    ScheduledExecutorService exec;
    ProgressBar progressBar;
    ProgressBar progressBar2;
    int iteration = 0;
    Runnable runnable;
    public static boolean perdu = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home);
        initialisation();
        gestionBouttonRecommencer();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                    tvClique = (TextView) v;
                    String valeur = ((TextView) v).getText().toString();
                    numeroCliquer = Integer.valueOf(valeur);
                    verifierNumero();
                    adapter = new AdapterNumero(HomeActivity.this, numeros);
                    gridView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        exec.shutdown();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(btnRecommencer.getVisibility() != View.VISIBLE)
            Shuffle();
    }

    private void Shuffle() {
        exec = Executors.newSingleThreadScheduledExecutor();
        runnable = new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        iteration += FREQUENCE_RAFRAICHISSEMENT;
                        progressBar.setProgress(progressBar.getProgress() - FREQUENCE_RAFRAICHISSEMENT);
                        progressBar2.setProgress(progressBar.getProgress() - FREQUENCE_RAFRAICHISSEMENT);
                        if(iteration == FREQUENCE_ALEATOIRE){
                            melangerNumeros();
                            if(progressBar.getRotation() == 0)
                                progressBar.setRotation(180);
                            else
                                progressBar.setRotation(0);
                            if(progressBar2.getRotation() == 180)
                                progressBar2.setRotation(0);
                            else
                                progressBar2.setRotation(180);

                            progressBar.setProgress(progressBar.getMax());
                            progressBar2.setProgress(progressBar.getProgress() - FREQUENCE_RAFRAICHISSEMENT);
                            iteration = 0;
                        }
                    }
                });
            }
        };
        exec.scheduleAtFixedRate(runnable, 0, FREQUENCE_RAFRAICHISSEMENT, TimeUnit.MILLISECONDS);
    }

    private void gestionBouttonRecommencer() {
        btnRecommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRecommencer.setVisibility(View.GONE);
                rl.setBackgroundColor(Color.WHITE);
                init();

                numberVoulu = 1;
                adapter.notifyDataSetChanged();
                melangerNumeros();
                progressBar.setProgress((int) FREQUENCE_ALEATOIRE);
                progressBar2.setProgress((int) FREQUENCE_ALEATOIRE);
                iteration =0;
                gridView.setEnabled(true);
                numeroCliquer = 0;
                perdu = false;
                exec = Executors.newSingleThreadScheduledExecutor();
                exec.scheduleAtFixedRate(runnable, 0, FREQUENCE_RAFRAICHISSEMENT, TimeUnit.MILLISECONDS);
            }
        });
    }

    private void initialisation() {
        btnRecommencer = (Button) findViewById(R.id.btnRecommencer);
        rl = (RelativeLayout) findViewById(R.id.rlGeeral);
        //tvNbVoulu = (TextView) findViewById(R.id.tvNumberVoulu);
        gridView = (GridView) findViewById(R.id.gridview);
        adapter = new AdapterNumero(this, numeros);
        gridView.setAdapter(adapter);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax((int) FREQUENCE_ALEATOIRE);
        progressBar.setProgress((int) FREQUENCE_ALEATOIRE);
        progressBar.setScaleY(3f);

        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar2.setMax((int) FREQUENCE_ALEATOIRE);
        progressBar2.setProgress((int) FREQUENCE_ALEATOIRE);
        progressBar2.setScaleY(3f);
        progressBar2.setRotation(180);
        gridView.setBackgroundColor(Color.BLACK);
        gridView.setVerticalSpacing(1);
        gridView.setHorizontalSpacing(1);
    }

    private void verifierNumero() {
        if(numeroCliquer == numberVoulu){
            numberVoulu++;
            remplacerNumeros();
        } else {
            rl = (RelativeLayout) findViewById(R.id.rlGeeral);
            rl.setBackgroundColor(Color.BLACK);
            btnRecommencer.setVisibility(View.VISIBLE);
            perdu = true;
            exec.shutdown();
            gridView.setEnabled(false);
        }
    }

    private synchronized void remplacerNumeros() {
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == numberVoulu - 2)
                numeros[i] = numeros[i] + numeros.length;
        }
    }

    private void init() {
        for (int i = 0; i < numeros.length; i++){
            numeros[i] = i + 1;
        }
    }


    private void melangerNumeros() {

        Random rnd = new Random();

        for (int i = numeros.length - 1; i > 0; i--){
            int index = rnd.nextInt(i + 1);
            int a = numeros[index];
            numeros[index] = numeros[i];
            numeros[i] = a;
        }
        adapter = new AdapterNumero(this, numeros);
        gridView.setAdapter(adapter);
    }
}
